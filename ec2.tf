resource "aws_instance" "web_host" {
  ami           = "${var.ami}"
  instance_type = "t2.nano"
  vpc_security_group_ids = [
  "${aws_security_group.web-node.id}"]
  subnet_id = "${aws_subnet.web_subnet.id}"
  tags = merge({
    environment = var.environment
    terragoat   = "true"
    }, {
    git_commit           = "d68d2897add9bc2203a5ed0632a5cdd8ff8cefb0"
    git_file             = "terraform/aws/ec2.tf"
  })
}

resource "aws_ebs_volume" "web_host_storasdddsaage" {
  # unencrypted volume
  availability_zone = "${var.region}a"
  #encrypted         = false  # Setting this causes the volume to be recreated on apply 
  size = 1
  tags = {
    git_commit           = "d3439f0f2af62f6fa3521e14d6c27819ef8f12e1"
    git_file             = "terraform/aws/ec2.tf"
  }
}

resource "aws_ebs_volume" "web_hosst_storage123" {
  # unencrypted volume
  availability_zone = "${var.region}a"
  #encrypted         = false  # Setting this causes the volume to be recreated on apply 
  size = 1
  tags = merge({
    environment = var.environment
    terragoat   = "true"
    }, {
    git_commit2           = "d68d2897add9bc2203a5ed0632a5cdd8ff8cefb0"
    git_file2             = "terraform/aws/ec2.tf"
  })
}

resource "azurerm_sql_server" "example" {
  name                         = "terragoat-sqlserver-${var.environment}${random_integer.rnd_int.result}"
  resource_group_name          = azurerm_resource_group.example.name
  location                     = azurerm_resource_group.example.location
  version                      = "12.0"
  administrator_login          = "ariel"
  administrator_login_password = "Aa12345678"
  tags = merge({
    environment = var.environment
    terragoat   = "true"
    }, {
    git_commit           = "81738b80d571fa3034633690d13ffb460e1e7dea"
    git_file             = "terraform/azure/sql.tf"
    git_last_modified_at = "2020-06-19 21:14:50"
    git_last_modified_by = "Adin.Ermie@outlook.com"
    git_modifiers        = "Adin.Ermie/nimrodkor"
    git_org              = "bridgecrewio"
    git_repo             = "terragoat"
    yor_trace            = "e5ec3432-e61f-4244-b59e-9ecc24ddd4cb"
  })
}